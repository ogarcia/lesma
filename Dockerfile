ARG ALPINE_VERSION=latest

FROM alpine:${ALPINE_VERSION}
RUN adduser -S -D -H -h /var/lib/lesma -s /sbin/nologin -G users -g lesma lesma && \
    install -d -m0755 /usr/lib/lesma && \
    install -d -m0755 -o100 -g100 /var/lib/lesma
COPY target/release/lesma /bin/lesma
COPY static /usr/lib/lesma/static/
COPY templates /usr/lib/lesma/templates/
EXPOSE 8000
ENV LESMA_ADDRESS="0.0.0.0" \
    LESMA_PORT=8000 \
    LESMA_HTTPS=false \
    LESMA_LOG_LEVEL=normal \
    LESMA_TEXT_SIZE_LIMIT=1MiB \
    LESMA_BINARY_SIZE_LIMIT=10MiB \
    LESMA_DEFAULT_LIMIT_PLAIN=0 \
    LESMA_DEFAULT_LIMIT_BROWSER=0 \
    LESMA_DEFAULT_EXPIRE_PLAIN=720 \
    LESMA_DEFAULT_EXPIRE_BROWSER=24 \
    LESMA_MAX_EXPIRE=720 \
    LESMA_STORAGE=/var/lib/lesma
VOLUME [ "/var/lib/lesma" ]
USER lesma
ENTRYPOINT [ "/bin/lesma" ]
