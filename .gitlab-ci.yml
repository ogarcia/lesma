default:
  image: alpine:latest

variables:
  QUAY_REGISTRY_URL: "quay.io"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/lesma/${CI_COMMIT_TAG}"
  ARTIFACTS: lesma-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}-linux-amd64
  PACKAGE_NAME: lesma-${CI_COMMIT_TAG}-linux-amd64.tar.xz
  PACKAGE_PATH: lesma-${CI_COMMIT_TAG}-linux-amd64

stages:
  - Test
  - Build
  - Package
  - Docker
  - Publish
  - Release

Test lesma:
  stage: Test
  image: rust:alpine
  script:
    - apk -U --no-progress add musl-dev
    - cargo test
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - target

Build lesma:
  stage: Build
  image: rust:alpine
  script:
    - apk -U --no-progress add musl-dev
    - cargo build --release --locked
  artifacts:
    name: ${ARTIFACTS}
    paths:
      - target/release/lesma
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - target

Package lesma:
  stage: Package
  script:
    - install -Dm755 "target/release/lesma" "${ARTIFACTS}/lesma"
    - install -Dm644 "lesma.toml.example" "${ARTIFACTS}/lesma.toml.example"
    - install -Dm644 "README.md" "${ARTIFACTS}/README.md"
    - install -Dm644 "LICENSE" "${ARTIFACTS}/LICENSE"
    - cp -r static templates "${ARTIFACTS}"
  artifacts:
    name: ${ARTIFACTS}
    paths:
      - ${ARTIFACTS}/
  rules:
    - if: $CI_COMMIT_TAG == null && $CI_PIPELINE_SOURCE != "merge_request_event"

Package lesma release:
  stage: Package
  script:
    - apk -U --no-progress add xz
    - install -Dm755 "target/release/lesma" "${PACKAGE_PATH}/lesma"
    - install -Dm644 "lesma.toml.example" "${PACKAGE_PATH}/lesma.toml.example"
    - install -Dm644 "README.md" "${PACKAGE_PATH}/README.md"
    - install -Dm644 "LICENSE" "${PACKAGE_PATH}/LICENSE"
    - cp -r static templates "${PACKAGE_PATH}"
    - tar cvJf "${PACKAGE_NAME}" "${PACKAGE_PATH}"
  artifacts:
    name: ${ARTIFACTS}
    paths:
      - ${PACKAGE_NAME}
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == "42779682"

Build and publish Docker image:
  stage: Docker
  image: docker
  services:
    - docker:dind
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME} .
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}
    - docker login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_TOKEN}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME} ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}
    - docker push ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}
    - docker login -u ${QUAY_USERNAME} -p ${QUAY_PASSWORD} ${QUAY_REGISTRY_URL}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME} ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}
    - docker push ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_NAME}
  rules:
    - if: $CI_COMMIT_TAG == null && $CI_PROJECT_ID == "42779682" && $CI_PIPELINE_SOURCE != "merge_request_event" && $CI_COMMIT_REF_PROTECTED == "true"

Build and publish release Docker image:
  stage: Docker
  image: docker
  services:
    - docker:dind
  script:
    - |
      semver() {
        NUM='0|[1-9][0-9]*'
        SEMVER_REGEX="^(${NUM})\\.(${NUM})\\.(${NUM})$"
        if [[ "${1}" =~ "${SEMVER_REGEX}" ]]; then
          MINOR_PATCH="${1#*.}"
          MAJOR="${1%%.*}"
          MINOR="${MINOR_PATCH%.*}"
          PATCH="${MINOR_PATCH#*.}"
          IMAGE="${2:-$CI_REGISTRY_IMAGE}"
          docker tag ${IMAGE}:latest ${IMAGE}:${MAJOR}
          docker tag ${IMAGE}:latest ${IMAGE}:${MAJOR}.${MINOR}
          docker push ${IMAGE}:${MAJOR}
          docker push ${IMAGE}:${MAJOR}.${MINOR}
        fi
      }
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} .
    - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} ${CI_REGISTRY_IMAGE}:latest
    - docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
    - docker push ${CI_REGISTRY_IMAGE}:latest
    - semver ${CI_COMMIT_TAG}
    - docker login -u ${DOCKERHUB_USERNAME} -p ${DOCKERHUB_TOKEN}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
    - docker push ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}
    - docker push ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
    - semver ${CI_COMMIT_TAG} ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
    - docker login -u ${QUAY_USERNAME} -p ${QUAY_PASSWORD} ${QUAY_REGISTRY_URL}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}
    - docker tag ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
    - docker push ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_TAG}
    - docker push ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
    - semver ${CI_COMMIT_TAG} ${QUAY_REGISTRY_URL}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == "42779682"

Publish lesma release:
  stage: Publish
  image: curlimages/curl:latest
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file ${PACKAGE_NAME} "${PACKAGE_REGISTRY_URL}/${PACKAGE_NAME}"
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == "42779682"

Release:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script: |
    release-cli create --name "Release ${CI_COMMIT_TAG}" --tag-name ${CI_COMMIT_TAG} \
      --assets-link "{\"name\":\"${PACKAGE_NAME}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${PACKAGE_NAME}\"}"
  rules:
    - if: $CI_COMMIT_TAG && $CI_PROJECT_ID == "42779682"
