/*
 * lesma.js
 * Copyright (C) 2017-2025 Óscar García Amor <ogarcia@connectical.com>
 *
 * Distributed under terms of the GNU GPLv3 license.
 */

// service worker
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("service-worker.js");
}

// prevent send empty lesmas
$("#lesmafrm").submit(function(event) {
  var expire = $("#expire").val();
  if (!$.trim($("#lesma").val()) || (expire | 0) != expire || expire < 1) {
    // textarea is empty or contains only white-space
    event.preventDefault();
  }
});

// clear lesma and maintain focus
$("#newlesma").click(function() {
  $("#lesma").val('').focus();
});

// click to copy
function copyToClipboard(newClip) {
  var copyToastElement = $("#copyToast");
  var copyToast = new bootstrap.Toast(copyToastElement, {'autohide': true, 'delay': 5000});
  navigator.clipboard.writeText(newClip).then(() => {
    $("#copyToastBody").text("Link copied to clipboard");
  }, () => {
    $("#copyToastBody").text("Something has gone wrong and it is not possible to copy the text to the clipboard, select it and copy it by hand");
  });
  copyToast.show();
}
if (document.getElementById("link")) {
  document.getElementById("link").addEventListener("touchend", function(e){
    copyToClipboard($("#link").text());
  }, false);
  document.getElementById("link").addEventListener("click", function(e){
    copyToClipboard($("#link").text());
  }, false);
}
