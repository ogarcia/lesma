{% extends "layout" %}
{% block body %}
<div class="mb-end">
<div class="d-flex justify-content-between bg-primary text-white">
  <div class="p-2 px-4">lesma(1)</div>
  <div class="p-2">LESMA v{{ version }}</div>
  <div class="p-2 px-4">lesma(1)</div>
</div>

<p class="text-primary pt-2">NAME</p>
<p class="ps-4">lesma - simple paste app friendly with browser and command
line</p>

<p class="text-primary">SYNOPSIS</p>
<pre class="ps-4">&lt;command&gt; | curl -F 'lesma=&lt;-' {{ uri | safe }}</pre>

<p class="text-primary">DESCRIPTION</p>
<p class="ps-4">add <strong>?limit=n</strong> to set a download count limit (0: unlimited; max: 255)<br>
add <strong>?expire=n</strong> to set an expiration time in hours (max: {{ max_expire }})<br>
add <strong>?password=p</strong> to set a password or obtain a password-protected lesma<br>
replace <strong>'lesma=<-'</strong> with <strong>'data=@file'</strong> to send binary files (max: {{ binary_size_limit }})<br>
add any extension to resulting url for syntax highlighting<br>
add <strong>?raw</strong> for plain file<br>
add <strong>#n-&lt;number&gt;</strong> for go directly to line number
anchor</p>

<p class="text-primary">DEFAULTS</p>
<p class="ps-4">Download count limit: {{ default_limit_plain }} plain; {{ default_limit_browser }} browser<br>
Expiration time: {{ default_expire_plain }} plain; {{ default_expire_browser }} browser</p>

<p class="text-primary">SPECIALS</p>
<p class="ps-4"><a href="{{ uri | safe }}/:help">/:help</a> show this page</p>

<p class="text-primary">EXAMPLES</p>
<p class="ps-2 text-primary">Basic usage</p>
<pre class="ps-4">
~$ cat lesma.rs | curl -F 'lesma=&lt;-' {{ uri | safe }}
   <a href="{{ uri | safe }}/lesma">{{ uri | safe }}/lesma</a>
~$ firefox <a href="{{ uri | safe }}/lesma.rs">{{ uri | safe }}/lesma.rs</a></pre>
<p class="ps-2 text-primary">Set a view / download limit</p>
<pre class="ps-4">
~$ cat lesma.rs | curl -F 'lesma=&lt;-' '{{ uri | safe }}?limit=1'
   <a href="{{ uri | safe }}/lesma">{{ uri | safe }}/lesma</a>
~$ curl <a href="{{ uri | safe }}/lesma.rs">{{ uri | safe }}/lesma.rs</a>
   Show lesma contents. Colored!
~$ curl <a href="{{ uri | safe }}/lesma.rs">{{ uri | safe }}/lesma.rs</a>
   lesma not found

Note that cloning lesmas over the web or sharing the link in a social
network or chat that has the preview enabled consumes a view. To avoid
consuming this view when sharing the link you can set a password.</pre>
<p class="ps-2 text-primary">Send text that can only be viewed once</p>
<pre class="ps-4">
~$ curl -F 'lesma=some text' '{{ uri | safe }}?limit=1'</pre>
<p class="ps-2 text-primary">Set an expiration time of one hour</p>
<pre class="ps-4">
~$ curl -F 'lesma=some text' '{{ uri | safe }}?expire=1'</pre>
<p class="ps-2 text-primary">Set a limit of ten views or an expiration time of one day</p>
<pre class="ps-4">
~$ curl -F 'lesma=some text' '{{ uri | safe }}?limit=10&expire=24'</pre>
<p class="ps-2 text-primary">Set a password-protected lesma</p>
<pre class="ps-4">
~$ curl -F 'lesma=some text' '{{ uri | safe }}?password=complexword'

Note that the password is entered as a URL option so special characters
must be transformed into <a href="https://en.wikipedia.org/wiki/URL_encoding">URL encoding</a>. To avoid this you can use the
following method.

~$ curl -F 'lesma=some text' -F 'password=some pass' {{ uri | safe }}</pre>
<p class="ps-2 text-primary">Get a password-protected lesma</p>
<pre class="ps-4">
~$ curl '{{ uri | safe }}/lesma?password=complexword'

If the password has special characters, it is easier to use the
following method.

~$ curl -F 'password=some pass' {{ uri | safe }}</pre>
<p class="ps-2 text-primary">Set a limit of one view or an expiration of one hour and password-protected</p>
<pre class="ps-4">
~$ curl -F 'lesma=text' '{{ uri | safe }}?limit=1&expire=1&password=pass'</pre>
<p class="ps-2 text-primary">Send parameters in the request body instead of in the URL</p>
<pre class="ps-4">
~$ curl -F 'lesma=text' -F 'limit=1' -F 'expire=1' -F 'password=pass' \
   {{ uri | safe }}</pre>
<p class="ps-2 text-primary">Send any binary file</p>
<pre class="ps-4">
~$ curl -F 'data=@file' {{ uri | safe }}

You can send <strong>limit</strong>, <strong>expire</strong> and <strong>password</strong> parameters just like with
text lesmas. This always generates a download link regardless of the
file you send.</pre>

<p class="text-primary">AUTHOR</p>
<p class="ps-4">Written by Óscar García Amor</p>

<p class="text-primary">COPYRIGHT</p>
<p class="ps-4">Copyright © 2017-2025 Óscar García Amor (<a
   href="https://ogarcia.me">https://ogarcia.me</a>).<br>
 Distributed under terms of the GNU GPLv3 license.</p>

<p class="text-primary">REPORTING BUGS</p>
<p class="ps-4"><a
   href="https://gitlab.com/ogarcia/lesma/issues">https://gitlab.com/ogarcia/lesma/issues</a></p>

<p class="text-primary">SEE ALSO</p>
<p class="ps-4"><a
   href="http://gitlab.com/ogarcia/lesma">http://gitlab.com/ogarcia/lesma</a></p>
</div>
{% endblock %}
