<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="google" content="notranslate">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="lesma">
    <link rel="manifest" href="/manifest.json"/>
    <meta name="description" content="Simple paste app friendly with browser and command line.">
    {%- if kind in [ "root", "help" ] %}
    <meta name="author" content="Óscar García Amor">
    {%- endif %}
    <meta name="keywords" content="lesma, paste, pastebin, sprunge, pastein, hastebin, friendly, command line">
    {%- if kind != "error" %}
    <meta name="url" content="{{ uri | safe }}" itemprop="url">
    <meta property="og:title" content="lesma">
    <meta property="og:url" content="{{ uri | safe }}">
    <meta property="og:description" content="Simple paste app friendly with browser and command line.">
    <meta property="og:image" content="{{ uri | safe }}/img/favicon-512.png">
    {%- endif %}
    <title>lesma{% if stem %}/{{ stem }}{% endif %}</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <!-- lesma CSS -->
    <link rel="stylesheet" href="{{ uri | safe }}/css/lesma.css">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="{{ uri | safe }}/img/favicon-512.png" sizes="512x512">
    <link rel="icon" type="image/png" href="{{ uri | safe }}/img/favicon-256.png" sizes="256x256">
    <link rel="icon" type="image/png" href="{{ uri | safe }}/img/favicon-128.png" sizes="128x128">
    <link rel="icon" type="image/png" href="{{ uri | safe }}/img/favicon-64.png" sizes="64x64">
    <link rel="icon" type="image/png" href="{{ uri | safe }}/img/favicon-32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ uri | safe }}/img/favicon-16.png" sizes="16x16">

  </head>
  <body>
    <div class="container-fluid {% if editable or kind in [ "error", "password", "link" ] %} full-height{% endif %}">
      {% block body %}{% endblock %}
    </div>

    <nav class="navbar fixed-bottom navbar-expand-sm navbar-dark bg-primary">
      <div class="container-fluid flex-row-reverse">
        <a class="navbar-brand btn" href="{{ uri | safe }}/:help">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg><br />
          lesma</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="offcanvas offcanvas-start text-bg-primary" tabindex="-1" id="navbarNav" aria-labelledby="offcanvasNavLabel">
          <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasNavLabel">lesma</h5>
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          </div>
          <ul class="navbar-nav text-center">
            <li class="nav-item">
              <button id="savelesma" type="submit" class="btn btn-primary" form="lesmafrm" {% if not editable %}disabled{% endif %}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save">
                  <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline>
                </svg><br />
                Save</button>
            </li>
            <li class="nav-item">
              <{% if editable %}button{% else %}a{% endif %} id="newlesma" class="btn btn-primary"{% if not editable %} href="{{ uri | safe }}/"{% endif %}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-square">
                  <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line>
                </svg><br />
                New{% if editable %}</button>{% else %}</a>{% endif %}
            </li>
            <li class="nav-item">
              <{% if editable or kind in [ "error", "help", "link", "password" ] %}button{% else %}a{% endif %} id="clonelesma" class="btn btn-primary"{% if editable or kind in [ "error", "help", "link", "password" ] %} disabled{% else %} href="{{ uri | safe }}/clone/{{ stem }}"{% endif %}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copy">
                  <rect x="9" y="9" width="13" height="13" rx="2" ry="2"></rect><path d="M5 15H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v1"></path>
                </svg><br />
              Clone{% if editable or kind in [ "error", "help", "link", "password" ] %}</button>{% else %}</a>{% endif %}
            </li>
            <li class="nav-item">
              <{% if editable or kind in [ "error", "help", "link", "password" ] %}button{% else %}a{% endif %} id="clonelesma" class="btn btn-primary"{% if editable or kind in [ "error", "help", "link", "password" ] %} disabled{% else %} href="?raw"{% endif %}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu">
                  <rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect><rect x="9" y="9" width="6" height="6"></rect>
                  <line x1="9" y1="1" x2="9" y2="4"></line><line x1="15" y1="1" x2="15" y2="4"></line><line x1="9" y1="20" x2="9" y2="23"></line>
                  <line x1="15" y1="20" x2="15" y2="23"></line><line x1="20" y1="9" x2="23" y2="9"></line><line x1="20" y1="14" x2="23" y2="14"></line>
                  <line x1="1" y1="9" x2="4" y2="9"></line><line x1="1" y1="14" x2="4" y2="14"></line>
                </svg><br />
              Raw{% if editable or kind in [ "error", "help", "link", "password" ] %}</button>{% else %}</a>{% endif %}
            </li>
            <li class="nav-item">
              <button type="button" id="lesmainfo" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#lesmaInfoModal" {% if editable or kind in [ "error", "help", "link", "password" ] %}disabled{% endif %}>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info">
                  <circle cx="12" cy="12" r="10"></circle><line x1="12" y1="16" x2="12" y2="12"></line><line x1="12" y1="8" x2="12.01" y2="8"></line>
                </svg><br />
                Info</button>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    {%- if not editable and not kind in [ "error", "help", "link", "password" ] %}
    <div class="modal fade" id="lesmaInfoModal" tabindex="-1" aria-labelledby="lesmaInfoModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h1 class="modal-title fs-5" id="lesmaInfoModalLabel">Info of lesma{% if stem %}/{{ stem }}{% endif %}</h1>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <dl class="row mb-0">
              <dt class="col-sm-4">Remaining views</dt>
              <dd class="col-sm-8">{% if remaining %}{{ remaining }}{% else %}n/a{% endif %}</dd>
              <dt class="col-sm-4">Expiration date</dt>
              <dd class="col-sm-8">{% if expire %}{{ expire | safe }}{% else %}n/a{% endif %}</dd>
              <dt class="col-sm-4">Size</dt>
              <dd class="col-sm-8 mb-0">{% if chars and len %}{{ chars }} char(s) / {{ len | filesizeformat }}{% else %}n/a{% endif %}</dd>
            </dl>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    {%- endif %}

    <!-- jQuery first, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.6.3.slim.min.js" integrity="sha256-ZwqZIVdD3iXNyGHbSYdsmWP//UBokj2FHAxKuSBKDSo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>

    <script src="{{ uri | safe }}/lesma.js"></script>
  </body>
</html>
