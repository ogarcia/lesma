//
// models.rs
// Copyright (C) 2023-2025 Óscar García Amor <ogarcia@connectical.com>
// Distributed under terms of the GNU GPLv3 license.
//

use rocket::data::Capped;
use rocket::fs::TempFile;
use rocket::serde::{Deserialize, Serialize};
use time::OffsetDateTime;

#[derive(FromForm)]
pub struct LesmaData<'d> {
    pub lesma: Option<String>,
    pub data: Option<Capped<TempFile<'d>>>,
    pub limit: Option<u8>,
    pub expire: Option<u32>,
    pub password: Option<String>
}

#[derive(FromForm)]
pub struct NewLesma {
    pub lesma: String
}

#[derive(FromForm)]
pub struct LesmaPassword {
    pub password: String
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct LesmaMeta {
    pub hash: String,
    pub password: Option<String>,
    pub name: String,
    pub binary: bool,
    pub limit: u8,
    pub current: u8,
    #[serde(with = "time::serde::rfc3339")]
    pub create: OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub expire: OffsetDateTime
}
