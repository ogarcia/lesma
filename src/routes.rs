//
// routes.rs
// Copyright (C) 2023-2025 Óscar García Amor <ogarcia@connectical.com>
// Distributed under terms of the GNU GPLv3 license.
//

use std::io::{Cursor, ErrorKind};
use std::path::PathBuf;

use rocket::Config;
use rocket::form::Form;
use rocket::fs::NamedFile;
use rocket::http::{ContentType, Header, Status};
use rocket::http::hyper::header::{CONTENT_DISPOSITION, LOCATION};
use rocket::http::uri::Host;
use rocket::request::{Outcome, Request, FromRequest};
use rocket::response::{self, content, Responder, Response};
use rocket::State;
use rocket_dyn_templates::{context, Template};
use time::macros::format_description as fd;

use crate::APP_VERSION;
use crate::LesmaConfig;
use crate::models::{LesmaData, LesmaPassword, NewLesma};
use lesma::{
    get_uri,
    human_default_expire,
    human_default_limit,
    human_max_expire,
    highligh_plain,
    into_html,
    Lesma,
    PLAIN_TEXT_AGENTS
};

const MANIFEST: &str = r##"{"name":"lesma","description":"Simple paste app friendly with browser and command line",
"short_name":"lesma","start_url":"/","icons":[
{"src":"/img/favicon-512.png","sizes":"512x512","type":"image/png"},
{"src":"/img/favicon-256.png","sizes":"256x256","type":"image/png"},
{"src":"/img/favicon-128.png","sizes":"128x128","type":"image/png"},
{"src":"/img/favicon-64.png","sizes":"64x64","type":"image/png"},
{"src":"/img/favicon-32.png","sizes":"32x32","type":"image/png"},
{"src":"/img/favicon-16.png","sizes":"16x16","type":"image/png"}],
"share_target":{"action":"/new","method":"POST","enctype": "application/x-www-form-urlencoded","params":{"text":"lesma"}},
"display":"standalone","theme_color": "#0d6efd","background_color":"#fff"}"##;

pub struct BinaryFile(lesma::BinaryFile);
pub struct PlainRedirect(String);
pub struct PlainTextAgent(bool);

#[derive(Responder)]
pub enum LesmaResponder {
    BinaryFile(BinaryFile),
    PlainRedirect(PlainRedirect),
    Template(Template)
}

#[derive(Responder)]
pub enum LesmaError {
    #[response(status = 400)]
    PlainBadRequest(Template),
    #[response(status = 400)]
    BadRequest(Template),
    #[response(status = 404)]
    PlainNotFound(Template),
    #[response(status = 404)]
    NotFound(Template),
    #[response(status = 500)]
    PlainInternalServerError(Template),
    #[response(status = 500)]
    InternalServerError(Template)
}

impl<'r> Responder<'r, 'static> for BinaryFile {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let mut response = self.0.1.respond_to(req)?;
        response.set_header(ContentType::Binary);
        response.set_header(Header::new(CONTENT_DISPOSITION.as_str(), format!("attachment; filename=\"{}\"", self.0.2)));
        Ok(response)
    }
}

#[rocket::async_trait]
impl<'r> Responder<'r, 'static> for PlainRedirect {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        let body = format!("{}\n", self.0);
        Response::build()
            .status(Status::SeeOther)
            .header(ContentType::Plain)
            .header(Header::new(LOCATION.as_str(), self.0))
            .sized_body(body.len(), Cursor::new(body))
            .ok()
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for PlainTextAgent {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, ()> {
        let user_agent = request.headers().get_one("user-agent");
        match user_agent {
            Some(user_agent) => {
                Outcome::Success(PlainTextAgent(PLAIN_TEXT_AGENTS.iter().any(|a| user_agent.contains(a))))
            },
            None => Outcome::Success(PlainTextAgent(false))
        }
    }
}

#[catch(default)]
pub fn default_catcher(status: Status, req: &Request<'_>) -> Template {
    let user_agent = req.headers().get_one("user-agent");
    let plain_text_agent = match user_agent {
        Some(user_agent) => PLAIN_TEXT_AGENTS.iter().any(|a| user_agent.contains(a)),
        None => false
    };
    if plain_text_agent {
        // Return error in plain text
        Template::render("plain", context! {
            text: status.to_string()
        })
    } else {
        // Return error in HTML
        Template::render("error", context! {
            kind: "error",
            uri: "",
            code: status.to_string()
        })
    }
}

#[catch(404)]
pub async fn not_found(req: &Request<'_>) -> Template {
    let user_agent = req.headers().get_one("user-agent");
    let plain_text_agent = match user_agent {
        Some(user_agent) => PLAIN_TEXT_AGENTS.iter().any(|a| user_agent.contains(a)),
        None => false
    };
    if plain_text_agent {
        // Return error in plain text
        Template::render("plain", context! {
            text: "These aren't the droids you are looking for"
        })
    } else {
        // Return error in HTML
        Template::render("error", context! {
            kind: "error",
            uri: "",
            code: "404",
            error: "These aren't the droids you are looking for"
        })
    }
}

#[catch(413)]
pub async fn too_large(req: &Request<'_>) -> Template {
    let user_agent = req.headers().get_one("user-agent");
    let plain_text_agent = match user_agent {
        Some(user_agent) => PLAIN_TEXT_AGENTS.iter().any(|a| user_agent.contains(a)),
        None => false
    };
    if plain_text_agent {
        // Return error in plain text
        Template::render("plain", context! {
            text: "Payload Too Large: The request is larger than the server is willing or able to process"
        })
    } else {
        // Return error in HTML
        Template::render("error", context! {
            kind: "error",
            uri: "",
            code: "413",
            error: "Payload Too Large",
            description: "The request is larger than the server is willing or able to process"
        })
    }
}

#[get("/")]
pub async fn get_root(config: &State<LesmaConfig>, host: &Host<'_>, plain_text_agent: PlainTextAgent) -> Template {
    // Get sanitized root URI
    let uri = get_uri(config.https, host, &config.domains);
    // Make a human representation of the default values
    let default_limit_plain = human_default_limit(config.default_limit_plain);
    let default_limit_browser = human_default_limit(config.default_limit_browser);
    let default_expire_plain = human_default_expire(config.default_expire_plain);
    let default_expire_browser = human_default_expire(config.default_expire_browser);
    let max_expire = human_max_expire(config.max_expire);
    if plain_text_agent.0 {
        Template::render("help_plain", context! {
            uri: uri,
            default_limit_plain: default_limit_plain,
            default_limit_browser: default_limit_browser,
            default_expire_plain: default_expire_plain,
            default_expire_browser: default_expire_browser,
            max_expire: max_expire,
            binary_size_limit: config.binary_size_limit.to_string()
        })
    } else {
        Template::render("index", context! {
            uri: uri,
            kind: "root",
            editable: true,
            default_limit_browser: config.default_limit_browser,
            default_expire_browser: config.default_expire_browser,
            max_expire: config.max_expire,
            lesma: ""
        })
    }
}

#[post("/?<limit>&<expire>&<password>", data = "<lesma>")]
#[allow(clippy::too_many_arguments)]
pub async fn post_root(conf: &Config, config: &State<LesmaConfig>, host: &Host<'_>, plain_text_agent: PlainTextAgent, limit: Option<u8>, expire: Option<u32>, password: Option<String>, mut lesma: Form<LesmaData<'_>>) -> Result<LesmaResponder, LesmaError> {
    // Get sanitized root URI
    let uri = get_uri(config.https, host, &config.domains);
    // Get limit
    let limit = lesma.limit.unwrap_or(limit.unwrap_or_else(|| {
        if plain_text_agent.0 {
            config.default_limit_plain
        } else {
            config.default_limit_browser
        }
    }));
    trace!("lesma limit: {}", limit);
    // Get expire
    let expire = lesma.expire.map(|e| {
        if e == 0 || e > config.max_expire {
            config.max_expire
        } else {
            e
        }
    }).or(expire.map(|e| {
        if e == 0 || e > config.max_expire {
            config.max_expire
        } else {
            e
        }
    })).unwrap_or_else(|| {
        if plain_text_agent.0 {
            config.default_expire_plain
        } else {
            config.default_expire_browser
        }
    });
    trace!("lesma expire: {}", expire);
    // Get password
    let password = lesma.password.clone().or(password);
    // Check if is a plain or binary lesma
    match &lesma.lesma {
        Some(plain) => {
            debug!("Seems a plain text lesma");
            // Prevents the creation of empty lesmas
            if plain.trim() == "" {
                return Ok(LesmaResponder::PlainRedirect(PlainRedirect(uri)))
            }
            let lesma = Lesma::new(config.storage.clone());
            match lesma.save_plain(limit, expire, password, plain) {
                Ok(()) => if plain_text_agent.0 {
                    Ok(LesmaResponder::PlainRedirect(PlainRedirect(format!("{}/{}", &uri, &lesma.id))))
                } else {
                    // In the browser, show link instead redirect
                    Ok(LesmaResponder::Template(Template::render("link", context! {
                              uri: &uri,
                              stem: &lesma.id,
                              kind: "link",
                              editable: false,
                              limit: limit,
                              expire: expire,
                              link: format!("{}/{}", uri, lesma.id)
                    })))
                },
                Err(err) => {
                    error!("Cannot create new plain lesma, {}", err.to_string());
                    if plain_text_agent.0 {
                        // Return error in plain text
                        Err(LesmaError::PlainInternalServerError(Template::render("plain", context! {
                            text: "Cannot create new lesma"
                        })))
                    } else {
                        // Return error in HTML
                        Err(LesmaError::InternalServerError(Template::render("error", context! {
                            kind: "error",
                            uri: uri,
                            code: "500",
                            error: "Cannot create new lesma"
                        })))
                    }
                }
            }
        },
        None => {
            debug!("Seems a binary lesma");
            match lesma.data.as_mut() {
                Some(data) => {
                    // Prevents the creation of empty binary lesmas
                    if data.is_empty() || data.name().is_none() {
                        return Ok(LesmaResponder::PlainRedirect(PlainRedirect(uri)))
                    }
                    let lesma = Lesma::new(config.storage.clone());
                    match lesma.save_data(&conf.temp_dir.relative(), limit, expire, password, data).await {
                        Ok(()) => Ok(LesmaResponder::PlainRedirect(PlainRedirect(format!("{}/{}", uri, lesma.id)))),
                        Err(err) => {
                            if err.to_string() == "File too large" {
                                if plain_text_agent.0 {
                                    // Return error in plain text
                                    return Err(LesmaError::PlainBadRequest(Template::render("plain", context! {
                                        text: "File too large"
                                    })))
                                } else {
                                    // Return error in HTML
                                    return Err(LesmaError::BadRequest(Template::render("error", context! {
                                        kind: "error",
                                        uri: uri,
                                        code: "400",
                                        error: "File too large"
                                    })))
                                }
                            }
                            error!("Cannot create new binary lesma, {}", err.to_string());
                            if plain_text_agent.0 {
                                // Return error in plain text
                                Err(LesmaError::PlainInternalServerError(Template::render("plain", context! {
                                    text: "Cannot create new lesma"
                                })))
                            } else {
                                // Return error in HTML
                                Err(LesmaError::InternalServerError(Template::render("error", context! {
                                    kind: "error",
                                    uri: uri,
                                    code: "500",
                                    error: "Cannot create new lesma"
                                })))
                            }
                        }
                    }
                },
                None => Ok(LesmaResponder::PlainRedirect(PlainRedirect(uri))) // No data posted redirect to root
            }
        }
    }
}

#[get("/<lesma>?<raw>&<password>")]
pub async fn get_lesma(config: &State<LesmaConfig>, host: &Host<'_>, plain_text_agent: PlainTextAgent, lesma: PathBuf, raw: bool, password: Option<String>) -> Result<LesmaResponder, LesmaError> {
    // Get sanitized root URI
    let uri = get_uri(config.https, host, &config.domains);
    // Get lesma without extension (safe to unwrap since it always have data)
    let lesma_stem = lesma.file_stem().unwrap().to_str().unwrap();
    // Get lesma extension
    let lesma_extension = lesma.extension();
    // Get lesma manager
    let lesma = Lesma::from(config.storage.clone(), lesma_stem);
    // Get lesma meta
    let lesma_meta = match lesma.read_meta() {
        Ok(lesma_meta) => lesma_meta,
        Err(err) => if err.kind() == ErrorKind::NotFound {
            if plain_text_agent.0 {
                // Return error in plain text
                return Err(LesmaError::PlainNotFound(Template::render("plain", context! {
                    text: "lesma not found"
                })))
            } else {
                // Return error in HTML
                return Err(LesmaError::NotFound(Template::render("error", context! {
                    kind: "error",
                    uri: uri,
                    code: "404",
                    error: "lesma not found"
                })))
            }
        } else {
            error!("Unable to get meta of lesma {}, {}", &lesma_stem.to_string(), err.to_string());
            if plain_text_agent.0 {
                // Return error in plain text
                return Err(LesmaError::PlainInternalServerError(Template::render("plain", context! {
                    text: "Unable to get lesma meta"
                })))
            } else {
                // Return error in HTML
                return Err(LesmaError::InternalServerError(Template::render("error", context! {
                    kind: "error",
                    uri: uri,
                    code: "500",
                    error: "Unable to get lesma meta"
                })))
            }
        }
    };
    if lesma_meta.binary {
        // Return binary data
        match lesma.read_binary_data(&lesma_meta, password).await {
            Ok(binary_data) => Ok(LesmaResponder::BinaryFile(BinaryFile(binary_data))),
            Err(err) => if err.to_string() == "lesma expired" {
                if plain_text_agent.0 {
                    // Return error in plain text
                    Err(LesmaError::PlainNotFound(Template::render("plain", context! {
                        text: "lesma not found"
                    })))
                } else {
                    // Return error in HTML
                    Err(LesmaError::NotFound(Template::render("error", context! {
                        kind: "error",
                        uri: uri,
                        code: "404",
                        error: "lesma not found"
                    })))
                }
            } else if err.to_string() == "invalid password" {
                if plain_text_agent.0 {
                    // Return error in plain text
                    Err(LesmaError::PlainBadRequest(Template::render("plain", context! {
                        text: "You don't have the magic word"
                    })))
                } else {
                    // Return error / password ask in HTML
                    Err(LesmaError::BadRequest(Template::render("password", context! {
                        kind: "password",
                        uri: uri,
                        stem: lesma_stem
                    })))
                }
            } else {
                error!("Unable to get binary lesma {} (HASH: {}), {}", &lesma_stem.to_string(), &lesma.hash, err.to_string());
                if plain_text_agent.0 {
                    // Return error in plain text
                    Err(LesmaError::PlainInternalServerError(Template::render("plain", context! {
                        text: "Unable to get lesma data"
                    })))
                } else {
                    // Return error in HTML
                    Err(LesmaError::InternalServerError(Template::render("error", context! {
                        kind: "error",
                        uri: uri,
                        code: "500",
                        error: "Unable to get lesma data"
                    })))
                }
            }
        }
    } else {
        // Return plain data
        match lesma.read_plain_data(&lesma_meta, password) {
            Ok(plain_data) => {
                if (plain_text_agent.0 && lesma_extension.is_none()) || raw {
                    // Return raw
                    Ok(LesmaResponder::Template(Template::render("plain", context! {
                        text: plain_data
                    })))
                } else if plain_text_agent.0 {
                    // Return plain colorized (extension is safe to unwrap since it has checked before)
                    let highlighted_plain = highligh_plain(&plain_data, lesma_extension.unwrap());
                    Ok(LesmaResponder::Template(Template::render("plain", context! {
                        text: highlighted_plain
                    })))
                } else {
                    // Return HTML
                    let html = into_html(&plain_data, lesma_extension);
                    Ok(LesmaResponder::Template(Template::render("lesma", context! {
                        uri: uri,
                        stem: lesma_stem,
                        kind: "lesma",
                        editable: false,
                        chars: plain_data.chars().count(),
                        len: plain_data.len(),
                        remaining: if lesma_meta.limit == 0 {
                            "Unlimited".to_string()
                        } else if (lesma_meta.current + 1) > lesma_meta.limit {
                            // This case should never occur
                            "0".to_string()
                        } else {
                            (lesma_meta.limit - (lesma_meta.current + 1)).to_string()
                        },
                        // Get the expire date in human-readable form (safe to unwrap since the format is fixed here)
                        expire: lesma_meta.expire.format(fd!("[day]/[month]/[year] [hour]:[minute]:[second] UTC")).unwrap(),
                        lesma: html
                    })))
                }
            },
            Err(err) => if err.to_string() == "lesma expired" {
                if plain_text_agent.0 {
                    // Return error in plain text
                    Err(LesmaError::PlainNotFound(Template::render("plain", context! {
                        text: "lesma not found"
                    })))
                } else {
                    // Return error in HTML
                    Err(LesmaError::NotFound(Template::render("error", context! {
                        kind: "error",
                        uri: uri,
                        code: "404",
                        error: "lesma not found"
                    })))
                }
            } else if err.to_string() == "invalid password" {
                if plain_text_agent.0 {
                    // Return error in plain text
                    Err(LesmaError::PlainBadRequest(Template::render("plain", context! {
                        text: "You don't have the magic word"
                    })))
                } else {
                    // Return error / password ask in HTML
                    Err(LesmaError::BadRequest(Template::render("password", context! {
                        kind: "password",
                        uri: uri,
                        raw: raw,
                        stem: lesma_stem
                    })))
                }
            } else {
                error!("Unable to get plain lesma {} (HASH: {}), {}", &lesma_stem.to_string(), &lesma.hash, err.to_string());
                if plain_text_agent.0 {
                    // Return error in plain text
                    Err(LesmaError::PlainInternalServerError(Template::render("plain", context! {
                        text: "Unable to get lesma data"
                    })))
                } else {
                    // Return error in HTML
                    Err(LesmaError::InternalServerError(Template::render("error", context! {
                        kind: "error",
                        uri: uri,
                        code: "500",
                        error: "Unable to get lesma data"
                    })))
                }
            }
        }
    }
}


#[post("/<lesma>?<raw>", data = "<lesma_password>")]
pub async fn post_lesma(config: &State<LesmaConfig>, host: &Host<'_>, plain_text_agent: PlainTextAgent, lesma: PathBuf, raw: bool, lesma_password: Form<LesmaPassword>) -> Result<LesmaResponder, LesmaError> {
    get_lesma(config, host, plain_text_agent, lesma, raw, Some(lesma_password.password.clone())).await
}

#[get("/clone/<lesma>?<password>")]
pub async fn get_clone_lesma(config: &State<LesmaConfig>, host: &Host<'_>, lesma: PathBuf, password: Option<String>) -> Result<Template, LesmaError> {
    // Get sanitized root URI
    let uri = get_uri(config.https, host, &config.domains);
    // Get lesma without extension (safe to unwrap since it always have data)
    let lesma_stem = lesma.file_stem().unwrap().to_str().unwrap();
    // Get lesma manager
    let lesma = Lesma::from(config.storage.clone(), lesma_stem);
    // Get lesma meta
    let lesma_meta = match lesma.read_meta() {
        Ok(lesma_meta) => lesma_meta,
        Err(err) => if err.kind() == ErrorKind::NotFound {
            return Err(LesmaError::NotFound(Template::render("error", context! {
                kind: "error",
                uri: uri,
                code: "404",
                error: "lesma not found"
            })))
        } else {
            error!("Unable to get meta of lesma {}, {}", &lesma_stem.to_string(), err.to_string());
            return Err(LesmaError::InternalServerError(Template::render("error", context! {
                kind: "error",
                uri: uri,
                code: "500",
                error: "Unable to get lesma meta"
            })))
        }
    };
    if lesma_meta.binary {
        return Err(LesmaError::BadRequest(Template::render("error", context! {
            kind: "error",
            uri: uri,
            code: "400",
            error: "It is not possible to clone a binary lesma"
        })))
    }
    let plain_data = match lesma.read_plain_data(&lesma_meta, password) {
        Ok(plain_data) => plain_data,
        Err(err) => if err.to_string() == "lesma expired" {
            return Err(LesmaError::NotFound(Template::render("error", context! {
                kind: "error",
                uri: uri,
                code: "404",
                error: "lesma not found"
            })))
        } else if err.to_string() == "invalid password" {
            return Err(LesmaError::BadRequest(Template::render("password", context! {
                kind: "password",
                uri: uri,
                stem: lesma_stem,
                clone: true
            })))
        } else {
            error!("Unable to get plain lesma {} (HASH: {}), {}", &lesma_stem.to_string(), &lesma.hash, err.to_string());
            return Err(LesmaError::InternalServerError(Template::render("error", context! {
                kind: "error",
                uri: uri,
                code: "500",
                error: "Unable to get lesma data"
            })))
        }
    };
    Ok(Template::render("index", context! {
        uri: uri,
        kind: "root",
        editable: true,
        default_limit_browser: config.default_limit_browser,
        default_expire_browser: config.default_expire_browser,
        max_expire: config.max_expire,
        lesma: plain_data
    }))
}

#[post("/clone/<lesma>", data = "<lesma_password>")]
pub async fn post_clone_lesma(config: &State<LesmaConfig>, host: &Host<'_>, lesma: PathBuf, lesma_password: Form<LesmaPassword>) -> Result<Template, LesmaError> {
    get_clone_lesma(config, host, lesma, Some(lesma_password.password.clone())).await
}

#[post("/new", data = "<new_lesma>")]
pub async fn post_new_lesma(config: &State<LesmaConfig>, host: &Host<'_>, new_lesma: Form<NewLesma>) -> Template {
    // Get sanitized root URI
    let uri = get_uri(config.https, host, &config.domains);
    Template::render("index", context! {
        uri: uri,
        kind: "root",
        editable: true,
        default_limit_browser: config.default_limit_browser,
        default_expire_browser: config.default_expire_browser,
        max_expire: config.max_expire,
        lesma: new_lesma.lesma.clone()
    })
}

#[get("/:help?<raw>")]
pub async fn get_help(config: &State<LesmaConfig>, host: &Host<'_>, plain_text_agent: PlainTextAgent, raw: bool) -> Template {
    // Get sanitized root URI
    let uri = get_uri(config.https, host, &config.domains);
    // Make a human representation of the default values
    let default_limit_plain = human_default_limit(config.default_limit_plain);
    let default_limit_browser = human_default_limit(config.default_limit_browser);
    let default_expire_plain = human_default_expire(config.default_expire_plain);
    let default_expire_browser = human_default_expire(config.default_expire_browser);
    let max_expire = human_max_expire(config.max_expire);
    if plain_text_agent.0 || raw {
        Template::render("help_full", context! {
            uri: uri,
            default_limit_plain: default_limit_plain,
            default_limit_browser: default_limit_browser,
            default_expire_plain: default_expire_plain,
            default_expire_browser: default_expire_browser,
            max_expire: max_expire,
            binary_size_limit: config.binary_size_limit.to_string(),
            version: format!("lesma v{:<64}lesma(1)", APP_VERSION)
        })
    } else {
        Template::render("help_html", context! {
            uri: uri,
            kind: "help",
            default_limit_plain: default_limit_plain,
            default_limit_browser: default_limit_browser,
            default_expire_plain: default_expire_plain,
            default_expire_browser: default_expire_browser,
            max_expire: max_expire,
            binary_size_limit: config.binary_size_limit.to_string(),
            version: APP_VERSION
        })
    }
}

#[get("/offline")]
pub async fn get_offline() -> Template {
    Template::render("offline", context! {})
}

#[get("/favicon.ico")]
pub async fn get_favicon(config: &State<LesmaConfig>) -> Option<NamedFile> {
    NamedFile::open(config.static_dir.join("favicon.ico")).await.ok()
}

#[get("/lesma.js")]
pub async fn get_lesma_js(config: &State<LesmaConfig>) -> Option<NamedFile> {
    NamedFile::open(config.static_dir.join("js").join("lesma.js")).await.ok()
}

#[get("/service-worker.js")]
pub async fn get_service_worker_js(config: &State<LesmaConfig>) -> Option<NamedFile> {
    NamedFile::open(config.static_dir.join("js").join("service-worker.js")).await.ok()
}

#[get("/manifest.json")]
pub async fn get_manifest() -> content::RawJson<&'static str> {
    content::RawJson(MANIFEST)
}
